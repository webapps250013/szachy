# szachy
System wizualizacji ruchów bierek szachowych w oparciu o moduł sterujący Arduino, aplikację dla systemu Android oraz serwer pośredniczący z bazą danych. Komponent webowy.

# System
System został zaprojektowany jako kompleksowe narzędzie do gry w szachy pomiędzy dwoma przeciwnikami znajdującymi się w różnych lokalizacjach, lecz chcących używać fizycznej szachownicy do tego celu. 

[Opis systemu](about.pdf)
